# Neural Networks FCIS Course Project

## Download
Either choose the download as zip option at the top of the page or if you have git installed on your machine issue the following command: `git clone https://github.com/lordadamson/NN.git`

## Dependencies
- python 2.7
- cPickle
- numpy

## Running the project
- `python main.py`
