#from random import random

import mnist_loader
import network
import numpy as np
from PIL import Image

def dataset_to_visiblearray(x):
	"""
	takes single image array from dataset and returns a visible 28 by 28 printable array.
	example: dataset_to_visiblearray(test_data[0][0])
	"""
	x = x.ravel()
	counter = 0
	for i in x:
		if i > 0:
			x[counter] = 1
		counter += 1
	return x.reshape(28, 28).astype(int)

def train():
	training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
	net = network.Network([784, 30, 10], cost=network.CrossEntropyCost)
	net.SGD(training_data, 30, 10, 0.5, lmbda = 5.0, evaluation_data=validation_data, monitor_evaluation_accuracy=True, monitor_evaluation_cost=True, monitor_training_accuracy=True, monitor_training_cost=True)

def image2pixelarray(filepath):
    im = Image.open(filepath).convert('L')
    image_list = list(im.getdata())
    counter = 0
    for i in image_list:
        image_list[counter] = [(1 - np.float(image_list[counter])/255)]
        counter += 1

    return np.array(image_list)

def format_input_data(data):
    #data = data.reshape((784, 1))
    test_inputs = [np.reshape(x, (784, 1)) for x in data[0]]
    test_data = zip(test_inputs, data[1])
    return test_data

if __name__ == '__main__':
	#train()
	training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
	net = network.load("9790.network")
	
	digits = [image2pixelarray(str(x) + ".png") for x in range(10)]
	
	images = [digits, range(10)]
	images = format_input_data(images)
	output = [net.feedforward(images[i][0]) for i in range(10)]
	
	for i in output:
		print np.argmax(i)
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
